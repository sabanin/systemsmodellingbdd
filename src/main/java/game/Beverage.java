package game;

import lombok.Data;

@Data
public class Beverage {
	String name;
	String quality;
	int volume;
	int cost;
	
}
