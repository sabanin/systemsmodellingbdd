package game;

import lombok.Data;

@Data
public class Employee {
	String name = "";
	String surname = "";
	String job = "";
	int experienceLevel = 0;
	int salary = 0;
	
	public void increaseLevel(){
		if (experienceLevel<3) experienceLevel++;
	}
}
