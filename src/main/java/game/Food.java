package game;

import lombok.Data;

@Data
public class Food {
	String name;
	String quality;
	int calories;
	int cost;
}
