package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Setter;

public class GameController {
	public static final int TRAIN_WAITER = 800;
	public static final int TRAIN_BARMEN = 1200;
	public static final int TRAIN_CHEF = 1200;

	Restaurant restaurant;
	Player player;
	@Setter
	int costOfLowDish, costOfLowBev, costOfHighDish, costOfHighBev;
	int expences, salary;

	public GameController(Restaurant restaurant, Player player) {
		this.restaurant = restaurant;
		this.player = player;
		expences = 0;
		salary = 0;
	}

	public void startGame() {
		System.out.println("Welcome to " + restaurant.getName() + " Game!");
		System.out.println("Thank you, " + player.getName() + ".");
		System.out.println("The starting budget equals to "
				+ restaurant.getBudget());
	}

	public void createMenu(List<Food> foods, List<Beverage> beverages) {
		try {
			// check Dishes
			String priceLow = "";
			String priceHigh = "";
			for (Food item : foods) {
				if (item.getQuality().equalsIgnoreCase("low")) {
					if (priceLow.isEmpty())
						priceLow = Integer.toString(item.getCost());
					else if (!priceLow.equals(Integer.toString(item.getCost())))
						throw new Exception(
								"ERROR!!! All the low quality dishes must have the same price");
				}
				if (item.getQuality().equalsIgnoreCase("high")) {
					if (priceHigh.isEmpty())
						priceHigh = Integer.toString(item.getCost());
					else if (!priceHigh
							.equals(Integer.toString(item.getCost())))
						throw new Exception(
								"ERROR!!! All the high quality dishes must have the same price");
				}
			}
			// check Beverages
			priceLow = "";
			priceHigh = "";
			for (Beverage item : beverages) {
				if (item.getQuality().equalsIgnoreCase("low")) {
					if (priceLow.isEmpty())
						priceLow = Integer.toString(item.getCost());
					else if (!priceLow.equals(Integer.toString(item.getCost())))
						throw new Exception(
								"ERROR!!! All the low quality beverages must have the same price");
				}
				if (item.getQuality().equalsIgnoreCase("high")) {
					if (priceHigh.isEmpty())
						priceHigh = Integer.toString(item.getCost());
					else if (!priceHigh
							.equals(Integer.toString(item.getCost())))
						throw new Exception(
								"ERROR!!! All the high quality beverages must have the same price");
				}
			}
			restaurant.setBeverages(beverages);
			restaurant.setFoods(foods);
			System.out.println("Restaurant menu is created");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public void processOrder(int numbOfClients, String order) {
		if (order.equalsIgnoreCase("high quality dishes"))
			expences += (numbOfClients * costOfHighDish);
		if (order.equalsIgnoreCase("low quality dishes"))
			expences += (numbOfClients * costOfLowDish);
		if (order.equalsIgnoreCase("high quality beverages"))
			expences += (numbOfClients * costOfHighBev);
		if (order.equalsIgnoreCase("low quality beverages"))
			expences += (numbOfClients * costOfLowBev);
	}

	public void setEmployee(Employee employee) {
		restaurant.addEmployee(employee);
	}

	public void paySalaries() {
		for (Employee item : restaurant.getEmployees()) {
			restaurant.decreaseBudget(item.getSalary());
		}
	}

	public void setTables(List<Table> tables) {
		restaurant.setTables(tables);
	}

	public void clientsOnTable(int number, int clients) {
		restaurant.getTableByNumber(number).setClients(clients);
	}

	public void processAssignmentsTable(List<Table> tables) {
		try {
			Set<String> waiters = new HashSet<String>();
			for (Table table : tables) {
				waiters.add(table.getWaiter().getName());
			}

			for (String waiter : waiters) {
				int counter = 0;
				for (Table table : tables) {
					if (waiter.equalsIgnoreCase(table.getWaiter().getName()))
						counter++;
				}
				if (counter > 3)
					throw new Exception(waiter);
			}
			
			//assignment
			for (Table table: tables){
				restaurant.getTableByNumber(table.getNumber()).setWaiter(table.getWaiter());
			}
			System.out.println("Successful Assignment");
			
		} catch (Exception ex) {
			System.out.println("ERROR!!! " + ex.getMessage()
					+ " has more than three tables assigned");
		}
	}

	public void processLevelIncreasing(String[] names, String[] jobs) {
		int waitersDone = 0;
		int barmensDone = 0;
		int chefsDone = 0;
		int waitersFailed = 0;
		int barmensFailed = 0;
		int chefsFailed = 0;

		for (String name : names) {
			Employee employee = restaurant.getEmployeeByName(name);
			if (employee.getJob().equalsIgnoreCase("waiter")
					&& (restaurant.getBudget() > TRAIN_WAITER)) {
				restaurant.decreaseBudget(TRAIN_WAITER);
				employee.increaseLevel();
				waitersDone++;
			} else if (employee.getJob().equalsIgnoreCase("barman")
					&& (restaurant.getBudget() > TRAIN_BARMEN)) {
				restaurant.decreaseBudget(TRAIN_BARMEN);
				employee.increaseLevel();
				barmensDone++;
			} else if (employee.getJob().equalsIgnoreCase("chef")
					&& (restaurant.getBudget() > TRAIN_CHEF)) {
				restaurant.decreaseBudget(TRAIN_CHEF);
				employee.increaseLevel();
				chefsDone++;
			} else if (employee.getJob().equalsIgnoreCase("waiter")
					&& (restaurant.getBudget() < TRAIN_WAITER)) {
				waitersFailed++;
			} else if (employee.getJob().equalsIgnoreCase("barman")
					&& (restaurant.getBudget() < TRAIN_BARMEN)) {
				barmensFailed++;
			} else if (employee.getJob().equalsIgnoreCase("chef")
					&& (restaurant.getBudget() < TRAIN_CHEF)) {
				chefsFailed++;
			}
		}
		printMessage(waitersDone, waitersFailed, barmensDone, barmensFailed,
				chefsDone, chefsFailed);
	}

	private void printMessage(int wD, int wF, int bD, int bF, int cD, int cF) {
		boolean isFirst = true;
		String result = "";
		if (wD > 0) {
			if (isFirst)
				result += "T" + numbToString(wD, "waiter", true);
			else
				result += " and t" + numbToString(wD, "waiter", true);
			isFirst = false;
		}
		if (bD > 0) {
			if (isFirst)
				result += "T" + numbToString(bD, "barman", true);
			else
				result += " and t" + numbToString(bD, "barman", true);
			isFirst = false;
		}
		if (cD > 0) {
			if (isFirst)
				result += "T" + numbToString(cD, "chef", true);
			else
				result += " and t" + numbToString(cD, "chef", true);
			isFirst = false;
		}
		if (wF > 0) {
			if (isFirst)
				result += "T" + numbToString(wF, "waiter", false);
			else
				result += " and t" + numbToString(wF, "waiter", false);
			isFirst = false;
		}
		if (bF > 0) {
			if (isFirst)
				result += "T" + numbToString(bF, "barman", false);
			else
				result += " and t" + numbToString(bF, "barman", false);
			isFirst = false;
		}
		if (cF > 0) {
			if (isFirst)
				result += "T" + numbToString(cF, "chef", false);
			else
				result += " and t" + numbToString(cF, "chef", false);
			isFirst = false;
		}
		System.out.println(result);
	}

	private String numbToString(int i, String job, boolean succeeded) {
		if (i == 0)
			return "";
		String result = "he ";
		switch (i) {
		case 1:
			result += job;
			break;
		case 2:
			result += "two " + job + "s";
			break;
		case 3:
			result += "three " + job + "s";
			break;
		}
		if (succeeded)
			result += " experience level increased";
		else
			result += " experience level failed to increase";
		return result;
	}

	public Integer getBudget() {
		Integer budget = restaurant.getBudget() < 0 ? 0 : restaurant
				.getBudget();
		if (budget == 0)
			gameOver();
		return budget;
	}

	public void payExpences() {
		restaurant.decreaseBudget(expences);
		expences = 0;
	}

	public void gameOver() {
		System.out.println("Game Over");
	}
}
