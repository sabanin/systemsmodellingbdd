package game;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Restaurant {
	String name;
	Integer budget;
	List<Food> foods;
	List<Beverage> beverages;
	List<Employee> employees = new ArrayList<Employee>();
	List<Table> tables = new ArrayList<Table>();
	
	public Integer decreaseBudget(Integer value){
		budget -= value;
		return budget;
	}
	
	public void addEmployee(Employee emp){
		employees.add(emp);
	}
	
	public Employee getEmployeeByName(String name){
		for (Employee emp : employees){
			if (emp.getName().equalsIgnoreCase(name)){
				return emp;
			}
		}
		return null;
	}
	
	public Table getTableByNumber(int number){
		for (Table item : tables){
			if (item.getNumber() == number){
				return item;
			}
		}
		return null;
	}
}
