package game;

import lombok.Data;

@Data
public class Table {
	int number;
	int clients;
	Employee waiter;
}
