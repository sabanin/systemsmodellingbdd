package gameBudgetCalculation;

import lombok.Data;

@Data
public class EmployeeItem {
	String name;
	String surname;
	int experienceLevel;
	int salary;
	int taxCode;
}
