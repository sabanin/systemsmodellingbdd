package gameBudgetCalculation;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;


import game.Beverage;
import game.Employee;
import game.Food;
import game.GameController;
import game.Player;
import game.Restaurant;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.mockito.internal.verification.AtLeast;

import junit.framework.Assert;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GameBudgetCalculationSteps {
	
	Player player;
	Restaurant restaurant;
	GameController gc;
	PrintStream out;
	
	@Before
	public void setUp(){
		out = mock(PrintStream.class);
		System.setOut(out);
		player = new Player();
		restaurant = new Restaurant();
		gc=new GameController(restaurant, player);
	}
	
	@Given("^The game has started$")
	public void The_game_has_started() throws Throwable {
	    gc.startGame();
	}

	@Given("^The restaurant is created$")
	public void The_restaurant_is_created() throws Throwable {
	}

	@Given("^The budget is (\\d+)$")
	public void The_budget_is(int budget) throws Throwable {
	    restaurant.setBudget(budget);
	}

	@Given("^cost of ingredients for \"([^\"]*)\" is (\\d+)$")
	public void cost_of_ingredients_for_is(String what, int price) throws Throwable {
	    if (what.equalsIgnoreCase("high quality dishes")) gc.setCostOfHighDish(price);
	    if (what.equalsIgnoreCase("low quality dishes")) gc.setCostOfLowDish(price);
	    if (what.equalsIgnoreCase("high quality beverages")) gc.setCostOfHighBev(price);
	    if (what.equalsIgnoreCase("low quality beverages")) gc.setCostOfLowBev(price);
	}

	@Given("^(\\d+) clients choose \"([^\"]*)\"$")
	public void clients_choose(int numbOfClients, String order) throws Throwable {
	    gc.processOrder(numbOfClients, order);
	}

	@When("^The budget is updated based on the \"([^\"]*)\"$")
	public void The_budget_is_updated_based_on_the(String base) throws Throwable {
	    if(base.equalsIgnoreCase("expenses")) gc.payExpences();
	    if(base.equalsIgnoreCase("salary")) gc.paySalaries();
	}

	@Then("^The budget should be (\\d+)$")
	public void The_budget_should_be(int expect) throws Throwable {
	    Assert.assertEquals(expect, gc.getBudget().intValue());
	}

	@Then("^Game Over$")
	public void Game_Over() throws Throwable {
		gc.getBudget();
	    verify(out, atLeastOnce()).println("Game Over");
	}

	@Given("^The restaurant has the following employee\\(s\\) as \"([^\"]*)\"$")
	public void The_restaurant_has_the_following_employee_s_as(String place, List<EmployeeItem> employees) throws Throwable {
	    for (EmployeeItem item : employees){
	    	Employee emp = new Employee();
	    	emp.setName(item.getName());
	    	emp.setExperienceLevel(item.getExperienceLevel());
	    	emp.setSalary(item.getSalary());
	    	emp.setSurname(item.getSurname());
	    	gc.setEmployee(emp);
	    }
	}
}
