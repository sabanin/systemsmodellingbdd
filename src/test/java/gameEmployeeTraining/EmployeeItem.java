package gameEmployeeTraining;

import lombok.Data;

@Data
public class EmployeeItem {
	String name, job, experience;
}
