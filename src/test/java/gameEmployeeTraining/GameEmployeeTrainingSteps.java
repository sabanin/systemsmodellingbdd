package gameEmployeeTraining;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;
import game.Employee;
import game.GameController;
import game.Player;
import game.Restaurant;
import game.Table;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GameEmployeeTrainingSteps {
	
	Player player;
	Restaurant restaurant;
	GameController gc;
	PrintStream out;
	
	@Before
	public void setUp(){
		out = mock(PrintStream.class);
		System.setOut(out);
		player = new Player();
		restaurant = new Restaurant();
		gc=new GameController(restaurant, player);
	}
	
	@Given("^The game has started$")
	public void The_game_has_started() throws Throwable {
		gc.startGame();
	}

	@Given("^The restaurant is created$")
	public void The_restaurant_is_created() throws Throwable {
	}

	@Given("^The list of employees$")
	public void The_list_of_employees(List<EmployeeItem> employees) throws Throwable {
	    for (EmployeeItem item : employees){
	    	Employee employee = new Employee();
	    	employee.setName(item.getName());
	    	employee.setJob(item.getJob());
	    	if (item.getExperience().equalsIgnoreCase("low")) employee.setExperienceLevel(0);
	    	gc.setEmployee(employee);
	    }
	}

	@Given("^The budget is (\\d+)$")
	public void The_budget_is(int budget) throws Throwable {
	    restaurant.setBudget(budget);
	}

	@When("^I want to increase the experience of (\\d+) employee\\(s\\) \"([^\"]*)\" with the job \"([^\"]*)\"$")
	public void I_want_to_increase_the_experience_of_employee_s_with_the_job(int number, String name, String job) throws Throwable {
	    String[] names = name.split(",");
	    String[] jobs = name.split(",");
	    gc.processLevelIncreasing(names, jobs);
	}

	@Then("^I should see \"([^\"]*)\"$")
	public void I_should_see(String exp) throws Throwable {
	    verify(out, atLeastOnce()).println(exp);
	}

	@Then("^The budget should update to (\\d+)$")
	public void The_budget_should_update_to(int expected) throws Throwable {
	    Assert.assertEquals(expected, gc.getBudget().intValue());
	}
}
