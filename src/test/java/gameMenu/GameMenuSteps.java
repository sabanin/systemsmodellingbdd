package gameMenu;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import game.Beverage;
import game.Food;
import game.GameController;
import game.Player;
import game.Restaurant;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GameMenuSteps {
	
	List<Food> foods;
	List<Beverage> beverages; 
	Player player;
	Restaurant restaurant;
	GameController gc;
	PrintStream out;
	
	@Before
	public void setUp(){
		out = mock(PrintStream.class);
		System.setOut(out);
		player = new Player();
		restaurant = new Restaurant();
		gc=new GameController(restaurant, player);
		
		foods = new ArrayList<Food>();
		beverages = new ArrayList<Beverage>(); 
	}
	
	@Given("^The game has started$")
	public void The_game_has_started() throws Throwable {
	    //gc.startGame();
	}

	@Given("^The restaurant is created$")
	public void The_restaurant_is_created() throws Throwable {
	    
	}

	@Given("^the following set of \"([^\"]*)\"$")
	public void the_following_set_of(String itemName, List<MenuItem> items) throws Throwable {
		if (itemName.equalsIgnoreCase("beverages")){
			for (int i=0; i<items.size(); i++){
				Beverage bev = new Beverage();
				bev.setCost(items.get(i).getCost());
				bev.setName(items.get(i).getDishName());
				bev.setQuality(items.get(i).getQuality());
				bev.setVolume(items.get(i).getVolume());
				beverages.add(bev);
			}
		} else if (itemName.equalsIgnoreCase("dishes")){
				for (int i=0; i<items.size(); i++){
					Food food = new Food();
					food.setCost(items.get(i).getCost());
					food.setName(items.get(i).getBeverageName());
					food.setQuality(items.get(i).getQuality());
					food.setCalories(items.get(i).getCalories());
					foods.add(food);
				}
			}
	}

	@When("^I create a Menu$")
	public void I_create_a_Menu() throws Throwable {
	    gc.createMenu(foods, beverages);
	}

	@Then("^I should see the \"([^\"]*)\"$")
	public void I_should_see_the(String message) throws Throwable {
		verify(out, atLeastOnce()).println(message);
	}
}
