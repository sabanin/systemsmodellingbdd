package gameMenu;

import lombok.Data;

@Data
public class MenuItem {
	String dishName;
	String beverageName;
	String quality;
	int calories;
	int volume;
	int cost;
}
