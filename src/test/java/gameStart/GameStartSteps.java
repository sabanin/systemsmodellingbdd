package gameStart;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;

import java.io.PrintStream;

import game.GameController;
import game.Player;
import game.Restaurant;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GameStartSteps {
	
	Restaurant restaurant;
	Player player;
	GameController gameController;
	PrintStream out;
	
	@Before
	public void setUp(){
		out = mock(PrintStream.class);
		System.setOut(out);
		restaurant = new Restaurant();
		player = new Player();
	}
	
	@Given("^The \"([^\"]*)\" is created with the name \"([^\"]*)\"$")
	public void The_is_created_with_the_name(String entity, String name) throws Throwable {
	    if (entity.equalsIgnoreCase("restaurant")) {
		    restaurant.setName(name);
	    } else if (entity.equalsIgnoreCase("player")){
	    	player.setName(name);
	    }
	}

	@Given("^The restaurant budget is initialised to (\\d+)$")
	public void The_restaurant_budget_is_initialised_to(int budget) throws Throwable {
		restaurant.setBudget(new Integer(budget));
	}

	@When("^I start playing restaurant game$")
	public void I_start_playing_restaurant_game() throws Throwable {
	    gameController = new GameController(restaurant, player);
	    gameController.startGame();
	}

	@Then("^I should see \"([^\"]*)\"$")
	public void I_should_see(String exp) throws Throwable {
	    verify(out, atLeastOnce()).println(exp);
	}
	
}
