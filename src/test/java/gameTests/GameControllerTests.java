package gameTests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import game.Beverage;
import game.Employee;
import game.Food;
import game.GameController;
import game.Player;
import game.Restaurant;
import game.Table;

public class GameControllerTests {
	
	Restaurant restaurant;
	Player player;
	GameController gameController;
	PrintStream out;
	List<Food> foods;
	List<Beverage> beverages;
	List<Table> tables;
	List<Table> tryToAssign;
	
	@Before
	public void setUp(){
		out = mock(PrintStream.class);
		System.setOut(out);
		restaurant = new Restaurant();
		player = new Player();
		
		gameController=new GameController(restaurant, player);
		foods = new ArrayList<Food>();
		beverages = new ArrayList<Beverage>();
		tables = new ArrayList<Table>();
		tryToAssign = new ArrayList<Table>();
	}
	
	@Test
	public void gameStartTest1(){
		restaurant.setName("UT Restaurant (UTR)");
		player.setName("Naved");
		restaurant.setBudget(10000);
		gameController = new GameController(restaurant, player);
		gameController.startGame();
		verify(out, atLeastOnce()).println("Welcome to UT Restaurant (UTR) Game!");
		verify(out, atLeastOnce()).println("Thank you, Naved.");
		verify(out, atLeastOnce()).println("The starting budget equals to 10000");
	}
	
	@Test
	public void menuStepsTest1(){
		gameController.startGame();
		
		Food food1 = new Food();
		food1.setCost(12);
		food1.setName("grilled chicken");
		food1.setCalories(400);
		food1.setQuality("high");
		foods.add(food1);
		
		Food food2 = new Food();
		food2.setCost(7);
		food2.setName("lasagna");
		food2.setCalories(800);
		food2.setQuality("low");
		foods.add(food2);
		
		Food food3 = new Food();
		food3.setCost(12);
		food3.setName("gnocchi");
		food3.setCalories(700);
		food3.setQuality("high");
		foods.add(food3);

		Food food4 = new Food();
		food4.setCost(7);
		food4.setName("pizza");
		food4.setCalories(400);
		food4.setQuality("low");
		foods.add(food4);
		
		Food food5 = new Food();
		food5.setCost(12);
		food5.setName("snitzel");
		food5.setCalories(400);
		food5.setQuality("high");
		foods.add(food5);
		
		Beverage beverage1 = new Beverage();
		beverage1.setCost(5);
		beverage1.setName("coke");
		beverage1.setQuality("high");
		beverage1.setVolume(35);
		beverages.add(beverage1);
		
		Beverage beverage2 = new Beverage();
		beverage2.setCost(2);
		beverage2.setName("fanta");
		beverage2.setQuality("low");
		beverage2.setVolume(35);
		beverages.add(beverage2);
		
		Beverage beverage3 = new Beverage();
		beverage3.setCost(5);
		beverage3.setName("wine");
		beverage3.setQuality("high");
		beverage3.setVolume(50);
		beverages.add(beverage3);
		
		Beverage beverage4 = new Beverage();
		beverage4.setCost(2);
		beverage4.setName("beer");
		beverage4.setQuality("low");
		beverage4.setVolume(50);
		beverages.add(beverage4);
		
		Beverage beverage5 = new Beverage();
		beverage5.setCost(5);
		beverage5.setName("sprite");
		beverage5.setQuality("high");
		beverage5.setVolume(35);
		beverages.add(beverage5);
		
		gameController.createMenu(foods, beverages);
		verify(out, atLeastOnce()).println("Restaurant menu is created");
	}
	
	@Test
	public void menuStepsTest2(){
		gameController.startGame();
		
		Food food1 = new Food();
		food1.setCost(12);
		food1.setName("grilled chicken");
		food1.setCalories(400);
		food1.setQuality("high");
		foods.add(food1);
		
		Food food2 = new Food();
		food2.setCost(7);
		food2.setName("lasagna");
		food2.setCalories(800);
		food2.setQuality("low");
		foods.add(food2);
		
		Food food3 = new Food();
		food3.setCost(10);
		food3.setName("gnocchi");
		food3.setCalories(700);
		food3.setQuality("high");
		foods.add(food3);

		Food food4 = new Food();
		food4.setCost(7);
		food4.setName("pizza");
		food4.setCalories(400);
		food4.setQuality("low");
		foods.add(food4);
		
		Food food5 = new Food();
		food5.setCost(12);
		food5.setName("snitzel");
		food5.setCalories(400);
		food5.setQuality("high");
		foods.add(food5);
		
		Beverage beverage1 = new Beverage();
		beverage1.setCost(5);
		beverage1.setName("coke");
		beverage1.setQuality("high");
		beverage1.setVolume(35);
		beverages.add(beverage1);
		
		Beverage beverage2 = new Beverage();
		beverage2.setCost(2);
		beverage2.setName("fanta");
		beverage2.setQuality("low");
		beverage2.setVolume(35);
		beverages.add(beverage2);
		
		Beverage beverage3 = new Beverage();
		beverage3.setCost(5);
		beverage3.setName("wine");
		beverage3.setQuality("high");
		beverage3.setVolume(50);
		beverages.add(beverage3);
		
		Beverage beverage4 = new Beverage();
		beverage4.setCost(2);
		beverage4.setName("beer");
		beverage4.setQuality("low");
		beverage4.setVolume(50);
		beverages.add(beverage4);
		
		Beverage beverage5 = new Beverage();
		beverage5.setCost(5);
		beverage5.setName("sprite");
		beverage5.setQuality("high");
		beverage5.setVolume(35);
		beverages.add(beverage5);
		
		gameController.createMenu(foods, beverages);
		verify(out, atLeastOnce()).println("ERROR!!! All the high quality dishes must have the same price");
	}
	@Test
	public void menuStepsTest3(){
		gameController.startGame();
		
		Food food1 = new Food();
		food1.setCost(12);
		food1.setName("grilled chicken");
		food1.setCalories(400);
		food1.setQuality("high");
		foods.add(food1);
		
		Food food2 = new Food();
		food2.setCost(7);
		food2.setName("lasagna");
		food2.setCalories(800);
		food2.setQuality("low");
		foods.add(food2);
		
		Food food3 = new Food();
		food3.setCost(12);
		food3.setName("gnocchi");
		food3.setCalories(700);
		food3.setQuality("high");
		foods.add(food3);

		Food food4 = new Food();
		food4.setCost(6);
		food4.setName("pizza");
		food4.setCalories(400);
		food4.setQuality("low");
		foods.add(food4);
		
		Food food5 = new Food();
		food5.setCost(12);
		food5.setName("snitzel");
		food5.setCalories(400);
		food5.setQuality("high");
		foods.add(food5);
		
		Beverage beverage1 = new Beverage();
		beverage1.setCost(5);
		beverage1.setName("coke");
		beverage1.setQuality("high");
		beverage1.setVolume(35);
		beverages.add(beverage1);
		
		Beverage beverage2 = new Beverage();
		beverage2.setCost(2);
		beverage2.setName("fanta");
		beverage2.setQuality("low");
		beverage2.setVolume(35);
		beverages.add(beverage2);
		
		Beverage beverage3 = new Beverage();
		beverage3.setCost(5);
		beverage3.setName("wine");
		beverage3.setQuality("high");
		beverage3.setVolume(50);
		beverages.add(beverage3);
		
		Beverage beverage4 = new Beverage();
		beverage4.setCost(2);
		beverage4.setName("beer");
		beverage4.setQuality("low");
		beverage4.setVolume(50);
		beverages.add(beverage4);
		
		Beverage beverage5 = new Beverage();
		beverage5.setCost(5);
		beverage5.setName("sprite");
		beverage5.setQuality("high");
		beverage5.setVolume(35);
		beverages.add(beverage5);
		
		gameController.createMenu(foods, beverages);
		verify(out, atLeastOnce()).println("ERROR!!! All the low quality dishes must have the same price");
	}
	@Test
	public void menuStepsTest4(){
		gameController.startGame();
		
		Food food1 = new Food();
		food1.setCost(12);
		food1.setName("grilled chicken");
		food1.setCalories(400);
		food1.setQuality("high");
		foods.add(food1);
		
		Food food2 = new Food();
		food2.setCost(7);
		food2.setName("lasagna");
		food2.setCalories(800);
		food2.setQuality("low");
		foods.add(food2);
		
		Food food3 = new Food();
		food3.setCost(12);
		food3.setName("gnocchi");
		food3.setCalories(700);
		food3.setQuality("high");
		foods.add(food3);

		Food food4 = new Food();
		food4.setCost(7);
		food4.setName("pizza");
		food4.setCalories(400);
		food4.setQuality("low");
		foods.add(food4);
		
		Food food5 = new Food();
		food5.setCost(12);
		food5.setName("snitzel");
		food5.setCalories(400);
		food5.setQuality("high");
		foods.add(food5);
		
		Beverage beverage1 = new Beverage();
		beverage1.setCost(7);
		beverage1.setName("coke");
		beverage1.setQuality("high");
		beverage1.setVolume(35);
		beverages.add(beverage1);
		
		Beverage beverage2 = new Beverage();
		beverage2.setCost(2);
		beverage2.setName("fanta");
		beverage2.setQuality("low");
		beverage2.setVolume(35);
		beverages.add(beverage2);
		
		Beverage beverage3 = new Beverage();
		beverage3.setCost(5);
		beverage3.setName("wine");
		beverage3.setQuality("high");
		beverage3.setVolume(50);
		beverages.add(beverage3);
		
		Beverage beverage4 = new Beverage();
		beverage4.setCost(2);
		beverage4.setName("beer");
		beverage4.setQuality("low");
		beverage4.setVolume(50);
		beverages.add(beverage4);
		
		Beverage beverage5 = new Beverage();
		beverage5.setCost(5);
		beverage5.setName("sprite");
		beverage5.setQuality("high");
		beverage5.setVolume(35);
		beverages.add(beverage5);
		
		gameController.createMenu(foods, beverages);
		verify(out, atLeastOnce()).println("ERROR!!! All the high quality beverages must have the same price");
	}
	@Test
	public void menuStepsTest5(){
		gameController.startGame();
		
		Food food1 = new Food();
		food1.setCost(12);
		food1.setName("grilled chicken");
		food1.setCalories(400);
		food1.setQuality("high");
		foods.add(food1);
		
		Food food2 = new Food();
		food2.setCost(7);
		food2.setName("lasagna");
		food2.setCalories(800);
		food2.setQuality("low");
		foods.add(food2);
		
		Food food3 = new Food();
		food3.setCost(12);
		food3.setName("gnocchi");
		food3.setCalories(700);
		food3.setQuality("high");
		foods.add(food3);

		Food food4 = new Food();
		food4.setCost(7);
		food4.setName("pizza");
		food4.setCalories(400);
		food4.setQuality("low");
		foods.add(food4);
		
		Food food5 = new Food();
		food5.setCost(12);
		food5.setName("snitzel");
		food5.setCalories(400);
		food5.setQuality("high");
		foods.add(food5);
		
		Beverage beverage1 = new Beverage();
		beverage1.setCost(5);
		beverage1.setName("coke");
		beverage1.setQuality("high");
		beverage1.setVolume(35);
		beverages.add(beverage1);
		
		Beverage beverage2 = new Beverage();
		beverage2.setCost(1);
		beverage2.setName("fanta");
		beverage2.setQuality("low");
		beverage2.setVolume(35);
		beverages.add(beverage2);
		
		Beverage beverage3 = new Beverage();
		beverage3.setCost(5);
		beverage3.setName("wine");
		beverage3.setQuality("high");
		beverage3.setVolume(50);
		beverages.add(beverage3);
		
		Beverage beverage4 = new Beverage();
		beverage4.setCost(2);
		beverage4.setName("beer");
		beverage4.setQuality("low");
		beverage4.setVolume(50);
		beverages.add(beverage4);
		
		Beverage beverage5 = new Beverage();
		beverage5.setCost(5);
		beverage5.setName("sprite");
		beverage5.setQuality("high");
		beverage5.setVolume(35);
		beverages.add(beverage5);
		
		gameController.createMenu(foods, beverages);
		verify(out, atLeastOnce()).println("ERROR!!! All the low quality beverages must have the same price");
	}
	
	@Test
	public void gameRepCalc1(){
		gameController.startGame();
		restaurant.setBudget(6000);
		gameController.setCostOfHighDish(10);
		gameController.setCostOfLowDish(3);
		gameController.setCostOfHighBev(3);
		gameController.setCostOfLowBev(1);
		gameController.processOrder(4,"high quality dishes");
		gameController.processOrder(6,"low quality dishes");
		gameController.processOrder(6,"high quality beverages");
		gameController.processOrder(4,"low quality beverages");
		gameController.payExpences();
		Assert.assertEquals(5920, gameController.getBudget().intValue());
	}
	
	@Test
	public void gameRepCalc2(){
		gameController.startGame();
		restaurant.setBudget(60);
		gameController.setCostOfHighDish(10);
		gameController.setCostOfLowDish(3);
		gameController.setCostOfHighBev(3);
		gameController.setCostOfLowBev(1);
		gameController.processOrder(4,"high quality dishes");
		gameController.processOrder(3,"low quality dishes");
		gameController.processOrder(3,"high quality beverages");
		gameController.processOrder(2,"low quality beverages");
		gameController.payExpences();
		Assert.assertEquals(0, gameController.getBudget().intValue());
		verify(out, atLeastOnce()).println("Game Over");
	}
	
	@Test
	public void gameRepCalc3(){
		gameController.startGame();
		restaurant.setBudget(6000);
		gameController.setCostOfHighDish(10);
		gameController.setCostOfLowDish(3);
		gameController.setCostOfHighBev(3);
		gameController.setCostOfLowBev(1);
		gameController.processOrder(16,"high quality dishes");
		gameController.processOrder(2,"low quality dishes");
		gameController.processOrder(12,"high quality beverages");
		gameController.processOrder(6,"low quality beverages");
		gameController.payExpences();
		Assert.assertEquals(5792, gameController.getBudget().intValue());
	}
	@Test
	public void gameRepCalc4(){
		gameController.startGame();
		restaurant.setBudget(8000);
		
		Employee waiter1 = new Employee();
		waiter1.setName("Naved");
		waiter1.setSurname("Ahmed");
		waiter1.setExperienceLevel(0);
		waiter1.setSalary(200);
		gameController.setEmployee(waiter1);
		
		Employee waiter2 = new Employee();
		waiter2.setName("Fabrizio");
		waiter2.setSurname("Maggi");
		waiter2.setExperienceLevel(0);
		waiter2.setSalary(200);
		gameController.setEmployee(waiter2);
		
		Employee waiter3 = new Employee();
		waiter3.setName("Amnir");
		waiter3.setSurname("Hadachi");
		waiter3.setExperienceLevel(0);
		waiter3.setSalary(200);
		gameController.setEmployee(waiter3);
		
		Employee chef = new Employee();
		chef.setName("Luciano");
		chef.setSurname("Garcia");
		chef.setExperienceLevel(0);
		chef.setSalary(300);
		gameController.setEmployee(chef);
		
		Employee barman = new Employee();
		barman.setName("Abel");
		barman.setSurname("Armas");
		barman.setExperienceLevel(0);
		barman.setSalary(300);
		gameController.setEmployee(barman);
		
		gameController.paySalaries();
		Assert.assertEquals(6800, gameController.getBudget().intValue());
		
	}
	@Test
	public void gameRepCalc5(){
		gameController.startGame();
		
		Employee waiter1 = new Employee();
		waiter1.setName("Naved");
		waiter1.setSurname("Ahmed");
		waiter1.setExperienceLevel(0);
		waiter1.setSalary(200);
		gameController.setEmployee(waiter1);
		
		Employee waiter2 = new Employee();
		waiter2.setName("Fabrizio");
		waiter2.setSurname("Maggi");
		waiter2.setExperienceLevel(0);
		waiter2.setSalary(200);
		gameController.setEmployee(waiter2);
		
		Employee waiter3 = new Employee();
		waiter3.setName("Amnir");
		waiter3.setSurname("Hadachi");
		waiter3.setExperienceLevel(0);
		waiter3.setSalary(200);
		gameController.setEmployee(waiter3);
		
		Employee chef = new Employee();
		chef.setName("Luciano");
		chef.setSurname("Garcia");
		chef.setExperienceLevel(0);
		chef.setSalary(300);
		gameController.setEmployee(chef);
		
		Employee barman = new Employee();
		barman.setName("Abel");
		barman.setSurname("Armas");
		barman.setExperienceLevel(0);
		barman.setSalary(300);
		gameController.setEmployee(barman);
		
		restaurant.setBudget(1000);
		gameController.paySalaries();
		Assert.assertEquals(0, gameController.getBudget().intValue());
		verify(out, atLeastOnce()).println("Game Over");
	}
	
	@Test
	public void gameRepCalc6(){
		gameController.startGame();
		
		Employee waiter1 = new Employee();
		waiter1.setName("Naved");
		waiter1.setSurname("Ahmed");
		waiter1.setExperienceLevel(0);
		waiter1.setSalary(200);
		gameController.setEmployee(waiter1);
		
		Employee waiter2 = new Employee();
		waiter2.setName("Fabrizio");
		waiter2.setSurname("Maggi");
		waiter2.setExperienceLevel(1);
		waiter2.setSalary(300);
		gameController.setEmployee(waiter2);
		
		Employee waiter3 = new Employee();
		waiter3.setName("Amnir");
		waiter3.setSurname("Hadachi");
		waiter3.setExperienceLevel(2);
		waiter3.setSalary(400);
		gameController.setEmployee(waiter3);
		
		Employee chef = new Employee();
		chef.setName("Luciano");
		chef.setSurname("Garcia");
		chef.setExperienceLevel(1);
		chef.setSalary(400);
		gameController.setEmployee(chef);
		
		Employee barman = new Employee();
		barman.setName("Abel");
		barman.setSurname("Armas");
		barman.setExperienceLevel(2);
		barman.setSalary(500);
		gameController.setEmployee(barman);
		
		restaurant.setBudget(1800);
		gameController.paySalaries();
		Assert.assertEquals(0, gameController.getBudget().intValue());
		verify(out, atLeastOnce()).println("Game Over");
	}
	
	@Test
	public void gameEmployeeTrainingTest1(){
		gameController.startGame();
		
		Employee employee1 = new Employee();
		employee1.setName("Luciano");
		employee1.setJob("chef");
		employee1.setExperienceLevel(0);
		gameController.setEmployee(employee1);
		
		Employee employee2 = new Employee();
		employee2.setName("Naved");
		employee2.setJob("waiter");
		employee2.setExperienceLevel(0);
		gameController.setEmployee(employee2);
		
		Employee employee3 = new Employee();
		employee3.setName("Fabrizio");
		employee3.setJob("waiter");
		employee3.setExperienceLevel(0);
		gameController.setEmployee(employee3);
		
		Employee employee4 = new Employee();
		employee4.setName("Amnir");
		employee4.setJob("waiter");
		employee4.setExperienceLevel(0);
		gameController.setEmployee(employee4);
		
		Employee employee5 = new Employee();
		employee5.setName("Abel");
		employee5.setJob("barman");
		employee5.setExperienceLevel(0);
		gameController.setEmployee(employee5);
		
		restaurant.setBudget(10000);
		String[] names = {"Luciano"};
	    String[] jobs = {"chef"};
	    gameController.processLevelIncreasing(names, jobs);
	    verify(out, atLeastOnce()).println("The chef experience level increased");
	    Assert.assertEquals(8800, gameController.getBudget().intValue());
	    
	}
	@Test
	public void gameEmployeeTrainingTest2(){
		gameController.startGame();
		
		Employee employee1 = new Employee();
		employee1.setName("Luciano");
		employee1.setJob("chef");
		employee1.setExperienceLevel(0);
		gameController.setEmployee(employee1);
		
		Employee employee2 = new Employee();
		employee2.setName("Naved");
		employee2.setJob("waiter");
		employee2.setExperienceLevel(0);
		gameController.setEmployee(employee2);
		
		Employee employee3 = new Employee();
		employee3.setName("Fabrizio");
		employee3.setJob("waiter");
		employee3.setExperienceLevel(0);
		gameController.setEmployee(employee3);
		
		Employee employee4 = new Employee();
		employee4.setName("Amnir");
		employee4.setJob("waiter");
		employee4.setExperienceLevel(0);
		gameController.setEmployee(employee4);
		
		Employee employee5 = new Employee();
		employee5.setName("Abel");
		employee5.setJob("barman");
		employee5.setExperienceLevel(0);
		gameController.setEmployee(employee5);
		
		restaurant.setBudget(10000);
		String[] names = {"Abel"};
	    String[] jobs = {"barman"};
	    gameController.processLevelIncreasing(names, jobs);
	    verify(out, atLeastOnce()).println("The barman experience level increased");
	    Assert.assertEquals(8800, gameController.getBudget().intValue());
	    
	}
	@Test
	public void gameEmployeeTrainingTest3(){
		gameController.startGame();
		
		Employee employee1 = new Employee();
		employee1.setName("Luciano");
		employee1.setJob("chef");
		employee1.setExperienceLevel(0);
		gameController.setEmployee(employee1);
		
		Employee employee2 = new Employee();
		employee2.setName("Naved");
		employee2.setJob("waiter");
		employee2.setExperienceLevel(0);
		gameController.setEmployee(employee2);
		
		Employee employee3 = new Employee();
		employee3.setName("Fabrizio");
		employee3.setJob("waiter");
		employee3.setExperienceLevel(0);
		gameController.setEmployee(employee3);
		
		Employee employee4 = new Employee();
		employee4.setName("Amnir");
		employee4.setJob("waiter");
		employee4.setExperienceLevel(0);
		gameController.setEmployee(employee4);
		
		Employee employee5 = new Employee();
		employee5.setName("Abel");
		employee5.setJob("barman");
		employee5.setExperienceLevel(0);
		gameController.setEmployee(employee5);
		
		restaurant.setBudget(10000);
		String[] names = {"Naved"};
	    String[] jobs = {"waiter"};
	    gameController.processLevelIncreasing(names, jobs);
	    verify(out, atLeastOnce()).println("The waiter experience level increased");
	    Assert.assertEquals(9200, gameController.getBudget().intValue());
	    
	}
	@Test
	public void gameEmployeeTrainingTest4(){
		gameController.startGame();
		
		Employee employee1 = new Employee();
		employee1.setName("Luciano");
		employee1.setJob("chef");
		employee1.setExperienceLevel(0);
		gameController.setEmployee(employee1);
		
		Employee employee2 = new Employee();
		employee2.setName("Naved");
		employee2.setJob("waiter");
		employee2.setExperienceLevel(0);
		gameController.setEmployee(employee2);
		
		Employee employee3 = new Employee();
		employee3.setName("Fabrizio");
		employee3.setJob("waiter");
		employee3.setExperienceLevel(0);
		gameController.setEmployee(employee3);
		
		Employee employee4 = new Employee();
		employee4.setName("Amnir");
		employee4.setJob("waiter");
		employee4.setExperienceLevel(0);
		gameController.setEmployee(employee4);
		
		Employee employee5 = new Employee();
		employee5.setName("Abel");
		employee5.setJob("barman");
		employee5.setExperienceLevel(0);
		gameController.setEmployee(employee5);
		
		restaurant.setBudget(700);
		String[] names = {"Luciano"};
	    String[] jobs = {"chef"};
	    gameController.processLevelIncreasing(names, jobs);
	    verify(out, atLeastOnce()).println("The chef experience level failed to increase");
	    
	}
	@Test
	public void gameEmployeeTrainingTest5(){
		gameController.startGame();
		
		Employee employee1 = new Employee();
		employee1.setName("Luciano");
		employee1.setJob("chef");
		employee1.setExperienceLevel(0);
		gameController.setEmployee(employee1);
		
		Employee employee2 = new Employee();
		employee2.setName("Naved");
		employee2.setJob("waiter");
		employee2.setExperienceLevel(0);
		gameController.setEmployee(employee2);
		
		Employee employee3 = new Employee();
		employee3.setName("Fabrizio");
		employee3.setJob("waiter");
		employee3.setExperienceLevel(0);
		gameController.setEmployee(employee3);
		
		Employee employee4 = new Employee();
		employee4.setName("Amnir");
		employee4.setJob("waiter");
		employee4.setExperienceLevel(0);
		gameController.setEmployee(employee4);
		
		Employee employee5 = new Employee();
		employee5.setName("Abel");
		employee5.setJob("barman");
		employee5.setExperienceLevel(0);
		gameController.setEmployee(employee5);
		
		restaurant.setBudget(10000);
		String[] names = {"Amnir","Fabrizio"};
	    String[] jobs = {"waiter","waiter"};
	    gameController.processLevelIncreasing(names, jobs);
	    verify(out, atLeastOnce()).println("The two waiters experience level increased");
	    Assert.assertEquals(8400, gameController.getBudget().intValue()); 
	}
	@Test
	public void gameEmployeeTrainingTest6(){
		gameController.startGame();
		
		Employee employee1 = new Employee();
		employee1.setName("Luciano");
		employee1.setJob("chef");
		employee1.setExperienceLevel(0);
		gameController.setEmployee(employee1);
		
		Employee employee2 = new Employee();
		employee2.setName("Naved");
		employee2.setJob("waiter");
		employee2.setExperienceLevel(0);
		gameController.setEmployee(employee2);
		
		Employee employee3 = new Employee();
		employee3.setName("Fabrizio");
		employee3.setJob("waiter");
		employee3.setExperienceLevel(0);
		gameController.setEmployee(employee3);
		
		Employee employee4 = new Employee();
		employee4.setName("Amnir");
		employee4.setJob("waiter");
		employee4.setExperienceLevel(0);
		gameController.setEmployee(employee4);
		
		Employee employee5 = new Employee();
		employee5.setName("Abel");
		employee5.setJob("barman");
		employee5.setExperienceLevel(0);
		gameController.setEmployee(employee5);
		
		restaurant.setBudget(880);
		String[] names = {"Abel","Naved"};
	    String[] jobs = {"barman","waiter"};
	    gameController.processLevelIncreasing(names, jobs);
	    verify(out, atLeastOnce()).println("The waiter experience level increased and the barman experience level failed to increase");
	    Assert.assertEquals(80, gameController.getBudget().intValue());
	}
	
	@Test
	public void gameWaiterAssigmentTest1(){
		gameController.startGame();
		
		Table table1 = new Table();
		table1.setNumber(new Integer(1).intValue());
		tables.add(table1);
		Table table2 = new Table();
		table2.setNumber(new Integer(2).intValue());
		tables.add(table2);
		Table table3 = new Table();
		table3.setNumber(new Integer(3).intValue());
		tables.add(table3);
		Table table4 = new Table();
		table4.setNumber(new Integer(4).intValue());
		tables.add(table4);
		Table table5 = new Table();
		table5.setNumber(new Integer(5).intValue());
		tables.add(table5);
		Table table6 = new Table();
		table6.setNumber(new Integer(6).intValue());
		tables.add(table6);
		Table table7 = new Table();
		table7.setNumber(new Integer(7).intValue());
		tables.add(table7);
		Table table8 = new Table();
		table8.setNumber(new Integer(8).intValue());
		tables.add(table8);
		Table table9 = new Table();
		table9.setNumber(new Integer(9).intValue());
		tables.add(table9);
		gameController.setTables(tables);
		
		gameController.clientsOnTable(1,2);
		gameController.clientsOnTable(2,2);
		gameController.clientsOnTable(4,2);
		gameController.clientsOnTable(6,2);
		gameController.clientsOnTable(9,2);
		
		Employee waiter1 = new Employee();
		waiter1.setExperienceLevel(0);
    	waiter1.setSalary(200);
    	waiter1.setName("Naved");
    	gameController.setEmployee(waiter1);
    	
		Employee waiter2 = new Employee();
		waiter2.setExperienceLevel(0);
    	waiter2.setSalary(200);
    	waiter2.setName("Fabrizio");
    	gameController.setEmployee(waiter2);
    	
		Employee waiter3 = new Employee();
		waiter3.setExperienceLevel(0);
    	waiter3.setSalary(200);
    	waiter3.setName("Amnir");
    	gameController.setEmployee(waiter3);
    	
    	Table table11 = new Table();
    	table11.setNumber(2);
    	table11.setClients(2);
    	table11.setWaiter(restaurant.getEmployeeByName("Naved"));
    	tryToAssign.add(table11);
    	
    	Table table22 = new Table();
    	table22.setNumber(9);
    	table22.setClients(2);
    	table22.setWaiter(restaurant.getEmployeeByName("Naved"));
    	tryToAssign.add(table22);
    	
    	Table table33 = new Table();
    	table33.setNumber(4);
    	table33.setClients(2);
    	table33.setWaiter(restaurant.getEmployeeByName("Amnir"));
    	tryToAssign.add(table33);
    	
    	Table table44 = new Table();
    	table44.setNumber(6);
    	table44.setClients(2);
    	table44.setWaiter(restaurant.getEmployeeByName("Fabrizio"));
    	tryToAssign.add(table44);
    	
    	Table table55 = new Table();
    	table55.setNumber(1);
    	table55.setClients(2);
    	table55.setWaiter(restaurant.getEmployeeByName("Fabrizio"));
    	tryToAssign.add(table55);
    	
    	gameController.processAssignmentsTable(tryToAssign);
    	verify(out, atLeastOnce()).println("Successful Assignment");
	}
	
	@Test
	public void gameWaiterAssigmentTest2(){
		gameController.startGame();
		
		Table table1 = new Table();
		table1.setNumber(new Integer(1).intValue());
		tables.add(table1);
		Table table2 = new Table();
		table2.setNumber(new Integer(2).intValue());
		tables.add(table2);
		Table table3 = new Table();
		table3.setNumber(new Integer(3).intValue());
		tables.add(table3);
		Table table4 = new Table();
		table4.setNumber(new Integer(4).intValue());
		tables.add(table4);
		Table table5 = new Table();
		table5.setNumber(new Integer(5).intValue());
		tables.add(table5);
		Table table6 = new Table();
		table6.setNumber(new Integer(6).intValue());
		tables.add(table6);
		Table table7 = new Table();
		table7.setNumber(new Integer(7).intValue());
		tables.add(table7);
		Table table8 = new Table();
		table8.setNumber(new Integer(8).intValue());
		tables.add(table8);
		Table table9 = new Table();
		table9.setNumber(new Integer(9).intValue());
		tables.add(table9);
		gameController.setTables(tables);
		
		gameController.clientsOnTable(1,2);
		gameController.clientsOnTable(2,2);
		gameController.clientsOnTable(3,2);
		gameController.clientsOnTable(4,2);
		gameController.clientsOnTable(5,2);
		
		Employee waiter1 = new Employee();
		waiter1.setExperienceLevel(0);
    	waiter1.setSalary(200);
    	waiter1.setName("Naved");
    	waiter1.setSurname("Ahmed");
    	gameController.setEmployee(waiter1);
    	
		Employee waiter2 = new Employee();
		waiter2.setExperienceLevel(0);
    	waiter2.setSalary(200);
    	waiter2.setName("Fabrizio");
    	waiter2.setSurname("Maggi");
    	gameController.setEmployee(waiter2);
    	
		Employee waiter3 = new Employee();
		waiter3.setExperienceLevel(0);
    	waiter3.setSalary(200);
    	waiter3.setName("Amnir");
    	waiter1.setSurname("Hadachi");
    	gameController.setEmployee(waiter3);
    	
    	Table table11 = new Table();
    	table11.setNumber(1);
    	table11.setClients(2);
    	table11.setWaiter(restaurant.getEmployeeByName("Naved"));
    	tryToAssign.add(table11);
    	
    	Table table22 = new Table();
    	table22.setNumber(2);
    	table22.setClients(2);
    	table22.setWaiter(restaurant.getEmployeeByName("Naved"));
    	tryToAssign.add(table22);
    	
    	Table table33 = new Table();
    	table33.setNumber(3);
    	table33.setClients(2);
    	table33.setWaiter(restaurant.getEmployeeByName("Naved"));
    	tryToAssign.add(table33);
    	
    	Table table44 = new Table();
    	table44.setNumber(5);
    	table44.setClients(2);
    	table44.setWaiter(restaurant.getEmployeeByName("Fabrizio"));
    	tryToAssign.add(table44);
    	
    	Table table55 = new Table();
    	table55.setNumber(4);
    	table55.setClients(2);
    	table55.setWaiter(restaurant.getEmployeeByName("Naved"));
    	tryToAssign.add(table55);
    	
    	gameController.processAssignmentsTable(tryToAssign);
    	verify(out, atLeastOnce()).println("ERROR!!! Naved has more than three tables assigned");
	}
}

