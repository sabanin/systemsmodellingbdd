package gameWaiterAssignment;

import lombok.Data;

@Data
public class ClientAssignmentTable {
	int tableNumber;
	int number_of_clients;
}
