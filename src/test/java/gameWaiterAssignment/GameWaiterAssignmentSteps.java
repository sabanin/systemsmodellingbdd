package gameWaiterAssignment;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;
import game.Employee;
import game.GameController;
import game.Player;
import game.Restaurant;
import game.Table;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GameWaiterAssignmentSteps {
	
	Player player;
	Restaurant restaurant;
	GameController gc;
	PrintStream out;
	List<Table> tables;
	
	@Before
	public void setUp(){
		out = mock(PrintStream.class);
		System.setOut(out);
		player = new Player();
		restaurant = new Restaurant();
		gc=new GameController(restaurant, player);
		tables = new ArrayList<Table>();
	}
	
	@Given("^The game has started$")
	public void The_game_has_started() throws Throwable {
	    gc.startGame();
	}

	@Given("^The restaurant is created$")
	public void The_restaurant_is_created() throws Throwable {
	}

	@Given("^The restaurant has following tables$")
	public void The_restaurant_has_following_tables(List<String> tableNumbers) throws Throwable {
	    for (String number:tableNumbers){
	    	Table table = new Table();
	    	try{
	    	table.setNumber(new Integer(number).intValue());
	    	tables.add(table);
	    	} catch (Exception ex) {}
	    }
	    gc.setTables(tables);
	}

	@Given("^the following clients are randomly assigned to the tables$")
	public void the_following_clients_are_randomly_assigned_to_the_tables(List<ClientAssignmentTable> assigments) throws Throwable {
	    for (ClientAssignmentTable item : assigments){
	    	gc.clientsOnTable(item.getTableNumber(), item.getNumber_of_clients());
	    }
	}

	@Given("^The restaurant has (\\d+) waiters \"([^\"]*)\" all with experience level (\\d+) and salary (\\d+)$")
	public void The_restaurant_has_waiters_all_with_experience_level_and_salary(int arg1, String waiters, int exp, int salary) throws Throwable {
	    String nameSurnames[] = waiters.split(", ");
	    for (String nameSurname : nameSurnames){
	    	String name = nameSurname;
	    	String surname = "";
	    	if (nameSurname.split(" ").length>1)
	    	{
	    		name = nameSurname.split(" ")[0];
	    		surname = nameSurname.split(" ")[1];
	    	}
	    	Employee emp = new Employee();
	    	emp.setExperienceLevel(exp);
	    	emp.setSalary(salary);
	    	emp.setName(name);
	    	emp.setSurname(surname);
	    	gc.setEmployee(emp);
	    }
	}

	@When("^I assigned the following waiters to the table$")
	public void I_assigned_the_following_waiters_to_the_table(List<TableItem> assigments) throws Throwable {
	    List<Table> tryToAssign = new ArrayList<Table>();
		for (TableItem item : assigments){
	    	Table table = new Table();
	    	table.setNumber(item.getTableNumber());
	    	table.setClients(item.getNumber_of_clients());
	    	table.setWaiter(restaurant.getEmployeeByName(item.getAssigned_waiter()));
	    	tryToAssign.add(table);
	    }
		gc.processAssignmentsTable(tryToAssign);
	}

	@Then("^I should see the message \"([^\"]*)\"$")
	public void I_should_see_the_message(String msg) throws Throwable {
	    verify(out, atLeastOnce()).println(msg);
	}
}
