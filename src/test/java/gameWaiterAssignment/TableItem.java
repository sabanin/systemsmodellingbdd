package gameWaiterAssignment;

import lombok.Data;

@Data
public class TableItem {
	int tableNumber;
	int number_of_clients;
	String assigned_waiter;
}
