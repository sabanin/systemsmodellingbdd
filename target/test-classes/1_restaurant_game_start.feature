Feature: Restaurant starts game

As a player
I want to start a game
So that I can play the restaurant game.

Scenario: Create a restaurant for player
	Given The "restaurant" is created with the name "UT Restaurant (UTR)"
	 And The "player" is created with the name "Naved"
	 And The restaurant budget is initialised to 10000
   	When I start playing restaurant game
    Then I should see "Welcome to UT Restaurant (UTR) Game!"
     And I should see "Thank you, Naved."
     And I should see "The starting budget equals to 10000"
     